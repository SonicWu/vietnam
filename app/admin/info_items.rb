# -*- encoding : utf-8 -*-
ActiveAdmin.register InfoItem do
  menu :label => "旅游攻略子条目", :if => proc{ can?(:manage, InfoItem) }, :priority => 5

  form do |f|
    f.inputs do
      f.input :name
      #f.input :cover
      f.input :link
      f.input :info_id, :as => :select, :collection => Info.maps
      f.input :prior
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "链接", :link
    column "所属旅游攻略" do |post|
      if post.info_id
        info = Info.where(id: post.info_id)
        if info.any?
          link_to info.name, admin_info_path(info)
        end
      end
    end
    column :uuid
    column "优先级", :prior
    actions
  end

end
