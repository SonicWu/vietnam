# -*- encoding : utf-8 -*-
ActiveAdmin.register Homestay do
  menu :label => "家庭旅馆", :if => proc{ can?(:manage, Homestay) }, :priority => 3

  form do |f|
    f.inputs do
      f.input :name
      f.input :contact
      f.input :price
      f.input :cover
      f.input :city_id, :as => :select, :collection => City.has_homestays
      f.input :desc
      f.input :intro, :input_html => {:class => "ckeditor"}
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "照片" do |post|
      image_tag post.cover, height: "60"
    end
    column "所属城市" do |post|
      if post.city
        link_to post.city.name, admin_city_path(post.city)
      end
    end
    column "简介", :desc
    column :uuid
    actions
  end

end
