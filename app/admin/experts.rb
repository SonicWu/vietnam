# -*- encoding : utf-8 -*-
ActiveAdmin.register Expert do
  menu :label => "当地导游", :if => proc{ can?(:manage, Expert) }, :priority => 3

  form do |f|
    f.inputs do
      f.input :name
      f.input :cover
      f.input :city_id, :as => :select, :collection => City.has_experts
      f.input :desc
      f.input :intro, :input_html => {:class => "ckeditor"}
      f.input :contact, :input_html => {:class => "ckeditor"}
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "姓名", :name
    column "头像" do |post|
      image_tag post.cover, height: "60"
    end
    column "所属城市" do |post|
      if post.city
        link_to post.city.name, admin_city_path(post.city)
      end
    end
    column "简介", :desc
    column :uuid
    actions
  end

end
