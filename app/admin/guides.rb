# -*- encoding : utf-8 -*-
ActiveAdmin.register Guide do
  menu :label => "旅游手册", :if => proc{ can?(:manage, Guide) }, :priority => 7

  form do |f|
    f.inputs do
      f.input :question
      f.input :answer, :input_html => {:class => "ckeditor"}
      f.input :category, :as => :select, :collection => Guide::CATEGORIES.each_with_index.map {|name, idx| [name, idx]}
      f.input :prior
      f.input :hot
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "问题", :question
    column "分类" do |post|
      Guide::CATEGORIES[post.category]
    end
    column "优先级", :prior
    column "在网站首页显示", :hot
    actions
  end
end
