ActiveAdmin.register Contact do
  belongs_to :order

  index do
    render "index", context: self
  end
end
