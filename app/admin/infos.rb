# -*- encoding : utf-8 -*-
ActiveAdmin.register Info do
  menu :label => "旅游攻略", :if => proc{ can?(:manage, Info) }, :priority => 5
  form do |f|
    f.inputs do
      f.input :name
      f.input :cover
      f.input :link
      f.input :category, :as => :select, :collection => Info::CATEGORIES.each_with_index.map {|name, idx| [name, idx]}
      f.input :prior
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "封面" do |post|
      image_tag post.cover.thumb, height: "60"
    end
    column "链接", :link
    column :uuid
    column "优先级", :prior
    actions
  end
end
