# -*- encoding : utf-8 -*-
ActiveAdmin.register AlbumList do
  menu :label => "相册集", :if => proc{ can?(:manage, AlbumList) }, :priority => 8

  index do
    selectable_column
    column :id
    column "名称", :name
    column "优先级", :prior
    actions
  end

end
