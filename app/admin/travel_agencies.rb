# -*- encoding : utf-8 -*-
ActiveAdmin.register TravelAgency do
  menu :label => "旅行社", :if => proc{ can?(:manage, TravelAgency) }, :priority => 6

  form do |f|
    f.inputs do
      f.input :name
      f.input :contact_name
      f.input :contact_phone
      f.input :contact_email
      f.input :contact_qq
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "联系人", :contact_name
    column "联系电话", :contact_phone
    column "联系QQ", :contact_qq
    column "联系邮箱", :contact_email
    actions
  end

end
