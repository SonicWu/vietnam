ActiveAdmin.register Hotel do
  belongs_to :order
  belongs_to :flight

  show do
    render "show", context: self
  end

  index do
    render "index", context: self
  end
end
