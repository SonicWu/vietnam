# -*- encoding : utf-8 -*-
ActiveAdmin.register Site do
  menu :label => "网站", :if => proc{ can?(:manage, Site) }, :priority => 12

  form do |f|
    f.inputs do
      f.input :experts_intro, :input_html => {:class => "ckeditor"}
      f.input :homestays_intro, :input_html => {:class => "ckeditor"}
      f.input :about_us, :input_html => {:class => "ckeditor"}
      f.input :contact_us, :input_html => {:class => "ckeditor"}
      f.input :disclaimer, :input_html => {:class => "ckeditor"}
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "导游栏目介绍", :experts_intro
    column "家庭旅馆介绍", :homestays_intro
    column "关于我们", :about_us
    column "免责声明", :disclaimer
    column "联系我们", :contact_us
    actions
  end
end
