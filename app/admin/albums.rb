# -*- encoding : utf-8 -*-
ActiveAdmin.register Album do
  menu :label => "相册", :if => proc{ can?(:manage, Album) }, :priority => 9

  action_item only:[:show] do
    link_to "管理图片", photos_admin_album_path(album)
  end

  form(:html => {multipart: true}) do |f|
    f.inputs do
      f.input :name
      f.input :desc
      f.input :category, :as => :select, :collection => Album::CATEGORIES.each_with_index.map {|name, idx| [name, idx]}
      f.input :album_list
      f.input :sight_id, :as => :select, :collection => Sight.unscoped.order('created_at DESC').all
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    # column "描述", :desc
    column "类型" do |post|
      content = Album::CATEGORIES[post.category]
      if post.category == 3 and post.sight_id
        content += "  "
        content += link_to "[景点]", admin_sight_path(post.sight_id)
      end
      content.html_safe
    end
    column "相册集" do |post|
      if post.album_list
        link_to post.album_list.name, admin_album_list_path(post.album_list)
      end
    end
    actions do |post|
      link_to "管理图片", photos_admin_album_path(post)
    end
  end

  member_action :photos, :method => :get do
    @album = Album.find(params[:id])
    @photos = @album.photos
    render 'admin/albums/photos'
  end

  member_action :upload_photos, :method => :post do
    album = Album.find(params[:id])
    p = Photo.new

    p.name = ""#album.name
    p.desc = ""#album.name
    p.album = album
    p.file = params[:qqfile]
    p.save!

    respond_to do |format|
      format.js do
        render json: {success: true}
      end
      # unless @photo.save
      #   flash[:error] = 'Photo could not be uploaded'
      # end
      # format.js do
      #   render :text => render_to_string(:partial => 'photos/photo', :locals => {:photo => @photo})
      # end
    end
  end

  member_action :destroy_photos, :method => :delete do
    album = Album.find(params[:id])
    album.photos.destroy_all
    album.save

    redirect_to photos_admin_album_path(album)
  end
end
