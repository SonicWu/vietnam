# -*- encoding : utf-8 -*-
ActiveAdmin.register Area do
  menu :label => "区域", :if => proc{ can?(:manage, Area) }, :priority => 2

  form do |f|
    f.inputs do
      f.input :name
      f.input :desc, :as => :text, :input_html => { :class => 'autogrow', :rows => 10, :cols => 20 }
      f.input :woeid
      f.input :cover
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "封面" do |post|
      image_tag post.cover.thumb, height: "60"
    end
    column :uuid
    actions
  end

end
