# -*- encoding : utf-8 -*-
ActiveAdmin.register City do
  menu :label => "城市", :if => proc{ can?(:manage, City) }, :priority => 3

  form do |f|
    f.inputs do
      f.input :name
      f.input :cover
      f.input :map
      f.input :area_id, :as => :select, :collection => Area.all
      f.input :prior
      f.input :desc
      f.input :route_info, :input_html => {:class => "ckeditor"}
      f.input :hotel_info, :input_html => {:class => "ckeditor"}
      f.input :traffic_info, :input_html => {:class => "ckeditor"}
      f.input :content, :input_html => {:class => "ckeditor"}
      f.input :woeid
      f.input :has_experts
      f.input :has_homestays
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "封面" do |post|
      image_tag post.cover.thumb, height: "60"
    end
    column "所属地区" do |post|
      if post.area
        link_to post.area.name, admin_area_path(post.area)
      end
    end
    column "优先级", :prior
    column :uuid
    actions
  end

end
