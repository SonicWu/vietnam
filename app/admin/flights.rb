ActiveAdmin.register Flight do
  belongs_to :order

  show do
    render "show", context: self
  end
end
