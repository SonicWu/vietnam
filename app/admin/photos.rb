# -*- encoding : utf-8 -*-
ActiveAdmin.register Photo do
  menu :label => "照片", :if => proc{ can?(:manage, Photo) }, :priority => 10

  form do |f|
    f.inputs do
      f.input :album_id, :as => :select, :collection => Album.order("updated_at DESC")
      f.input :name
      f.input :desc
      f.input :link
      f.input :file
      f.input :horizental
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "图片" do |post|
      if post.file.thumb
        image_tag post.file.thumb, height: "60"
      elsif post.file
        image_tag post.file, height: "60"
      end
    end
    column "所属相册" do |post|
      if post.album_id
        link_to post.album.name, admin_album_path(post.album)
      end
    end
    column "链接", :link
    column "横向图片", :horizental
    column :uuid
    actions
  end
end
