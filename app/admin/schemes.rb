# -*- encoding : utf-8 -*-
ActiveAdmin.register Scheme do
  menu :label => "旅游线路", :if => proc{ can?(:manage, Scheme) }, :priority => 5
  # form :html => {:enctype => "multipart/form-data" } do |f|
  form do |f|
    f.inputs do
      f.input :name
      f.input :desc
      f.input :area_id, :as => :select, :collection => Area.all
      f.input :cover#, :as => :file, :hint => f.template.image_tag(f.object.cover.url(:thumb))
      f.input :content, :input_html => {:class => "ckeditor"}
      f.input :price
      f.input :discount
      f.input :hot
      f.input :sights
      f.input :travel_agency_id, :as => :select, :collection => TravelAgency.all
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "封面" do |post|
      image_tag post.cover.thumb, height: "60"
    end
    column "价格", :price
    column :uuid
    column "热门", :hot
    column "旅行社" do |post|
      if post.travel_agency
        link_to post.travel_agency.name, admin_travel_agency_path(post.travel_agency)
      end
    end
    actions
  end
end
