# -*- encoding : utf-8 -*-
ActiveAdmin.register Sight do
  menu :label => "景点", :if => proc{ can?(:manage, Sight) }, :priority => 4

  form do |f|
    f.inputs do
      f.input :name
      # f.input :category, :as => :select, :collection => Sight::CATEGORIES
      f.input :cover
      f.input :city_id, :as => :select, :collection => City.all
      f.input :desc, :as => :text, :input_html => { :class => 'autogrow', :rows => 10, :cols => 20 }
      f.input :content, :input_html => {:class => "ckeditor"}
    end

    f.buttons
  end

  index do
    selectable_column
    column :id
    column "名称", :name
    column "封面" do |post|
      image_tag post.cover.thumb, height: "60"
    end
    column "所属城市" do |post|
      if post.city
        link_to post.city.name, admin_city_path(post.city)
      end
    end
    column :uuid
    actions
  end

end
