# -*- encoding : utf-8 -*-
ActiveAdmin.register Order do
  menu :label => "订单", :if => proc{ can?(:manage, Order) }, :priority => 11

  show do
    render "show", context: self
  end

  controller do
    def index
      redirect_to orders_path
    end

    def new 
      redirect_to new_order_path
    end

    def edit
      redirect_to edit_order_path(id: params[:id])
    end
  end
end
