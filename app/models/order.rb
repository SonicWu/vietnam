# -*- encoding : utf-8 -*-
class Order < ActiveRecord::Base
  attr_accessible :scheme_id, :tour_time, :tour_members_count,
                  :tour_children_free, :tour_children_discount,
                  :tour_adult_count, :status, :category, :cancel_reason

  # status
  # 0: consult
  # 1: ordered
  # 2: cancel
  # 3: payed
  STATUS = %w(
    咨询订单
    待确认订单
    预定成功
    确认发团
    取消发团
    确认支付
    核查完毕
  )

  CATEGORIES = %w(
    旅游订单
    接机订单
  )

  belongs_to :scheme
  has_one :hotel, :dependent => :destroy
  has_one :flight, :dependent => :destroy
  has_many :contacts, :dependent => :destroy

  default_scope order("status ASC, tour_time ASC")

  def tour?
    self.category == 0
  end

  def pickup?
    self.category == 1
  end

  def category_desc
    CATEGORIES[category]
  end

  def status_desc
    STATUS[status]
  end

  # validates :tour_members_count, presence: true
  # validates :status, inclusion: { in: [0,1,2,3], message: "设置的订单状态无效" }
end

# == Schema Information
#
# Table name: orders
#
#  id                     :integer          not null, primary key
#  scheme_id              :integer          not null
#  tour_time              :datetime         not null
#  tour_members_count     :integer          not null
#  tour_adult_count       :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  tour_children_free     :integer          default(0)
#  tour_children_discount :integer          default(0)
#  status                 :integer          not null
#

