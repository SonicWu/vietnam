# -*- encoding : utf-8 -*-
class Scheme < ActiveRecord::Base
  attr_accessible :content, :cover, :discount, :hot, :name, :price,
                  :area_id, :desc, :sight_ids, :uuid, :travel_agency_id

  scope :random, order("RAND()")

  belongs_to :travel_agency
  belongs_to :area
  has_many :schemesights
  has_many :sights, :through => :schemesights

  mount_uploader :cover, PhotoUploader

  validates :area_id, presence: true
  # validates :cover, :name, presence: true
  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  before_validation do
    if self.uuid.blank? # and self.new_record?
      write_attribute(:uuid, SecureRandom.uuid)
    end
    true
  end
end

# == Schema Information
#
# Table name: schemes
#
#  id               :integer          not null, primary key
#  name             :string(255)      not null
#  cover            :string(255)      not null
#  price            :integer          default(0), not null
#  content          :text
#  discount         :integer
#  hot              :boolean          default(FALSE)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  area_id          :string(255)
#  desc             :string(255)      default("")
#  uuid             :string(255)      not null
#  travel_agency_id :integer
#

