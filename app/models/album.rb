# -*- encoding : utf-8 -*-

# category :
# 0 the big slide show on page top
# 1 the photo albums
# 2 the cover
# 3 the sights

class Album < ActiveRecord::Base
  has_many :photos, :dependent => :destroy
  belongs_to :album_list

  attr_accessible :desc, :name, :category, :sight_id, :album_list_id

  CATEGORIES = %w(
    首页滚动大图
    图片相册
    封面
    景点相册
    妹子图
    旅游包车
    当地导游
  )

  scope :slides, where(category: 0)
  scope :pictures, where(category: 1)
  scope :covers, where(category: 2)
  scope :sights, where(category: 3)
  scope :girls, where(category: 4)

  def self.slide
    slides.first
  end

  def pictures?
    self.category == 1
  end

  def sights?
    self.category == 3
  end
end

# == Schema Information
#
# Table name: albums
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  desc          :text
#  category      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  sight_id      :integer
#  album_list_id :integer
#
