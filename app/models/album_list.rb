class AlbumList < ActiveRecord::Base
  has_many :albums

  attr_accessible :name, :prior
end

# == Schema Information
#
# Table name: album_lists
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  prior      :integer          default(0)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

