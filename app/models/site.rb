# -*- encoding : utf-8 -*-
class Site < ActiveRecord::Base
  attr_accessible :about_us, :disclaimer, :contact_us, :experts_intro, :homestays_intro
end
