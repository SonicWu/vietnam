class Flight < ActiveRecord::Base
  attr_accessible :number, :airline, :arrive_at, :order_id

  validates :number, :arrive_at, :order_id, presence: true
end

# == Schema Information
#
# Table name: flights
#
#  id         :integer          not null, primary key
#  number     :string(255)      not null
#  airline    :string(255)      not null
#  arrive_at  :datetime         not null
#  order_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

