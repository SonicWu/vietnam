# -*- encoding : utf-8 -*-
class Contact < ActiveRecord::Base
  attr_accessible :name, :chinese_phone, :vietnamese_phone,
                  :passport_number, :qq, :wechat, :weibo,
                  :email, :order_id, :gender

  GENDER = ['女士', '先生']

  validates :name, :chinese_phone, presence: true

  def gender_desc
    gender.nil?? '' : GENDER[gender]
  end
end

# == Schema Information
#
# Table name: contacts
#
#  id               :integer          not null, primary key
#  name             :string(255)      not null
#  chinese_phone    :string(255)      not null
#  vietnamese_phone :string(255)
#  passport_number  :string(255)
#  qq               :string(255)
#  wechat           :string(255)
#  weibo            :string(255)
#  email            :string(255)
#  order_id         :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

