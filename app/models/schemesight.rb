# -*- encoding : utf-8 -*-
class Schemesight < ActiveRecord::Base
  attr_accessible :scheme_id, :sight_id

  belongs_to :sight
  belongs_to :scheme
end

# == Schema Information
#
# Table name: schemesights
#
#  id         :integer          not null, primary key
#  scheme_id  :integer
#  sight_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

