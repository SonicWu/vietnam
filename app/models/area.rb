# -*- encoding : utf-8 -*-
class Area < ActiveRecord::Base
  attr_accessible :name, :woeid, :cover, :uuid, :desc
  has_many :cities
  has_many :schemes

  mount_uploader :cover, PhotoUploader

  validate :cover, presence: true
  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  before_validation do
    if self.uuid.blank? # and self.new_record?
      write_attribute(:uuid, SecureRandom.uuid) 
    end
    true
  end
end

# == Schema Information
#
# Table name: areas
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  woeid      :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  cover      :string(255)      not null
#  uuid       :string(255)      not null
#  desc       :string(255)      default("")
#  weather    :text
#

