# -*- encoding : utf-8 -*-
class Guide < ActiveRecord::Base
  attr_accessible :question, :answer, :category, :prior, :hot

  CATEGORIES = %w(
    旅行出发
    海关入境
    旅行途中
    旅行购物
    旅游攻略
    旅游包车
  )

  default_scope order('prior DESC')

  scope :hot, where(:hot => true)
  scope :infos, where(category: 4)
  scope :charters, where(category: 5)
end

# == Schema Information
#
# Table name: guides
#
#  id         :integer          not null, primary key
#  question   :string(255)
#  answer     :text
#  category   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  prior      :integer          default(0)
#  hot        :boolean          default(FALSE)
#

