# -*- encoding : utf-8 -*-
class Photo < ActiveRecord::Base
  belongs_to :album # , :polymorphic => true
  # attr_readonly :uuid
  # attr_protected :uuid
  attr_accessible :album_id, :desc, :name, :file, :link, :uuid, :horizental

  scope :horizental, where(horizental: true)

  mount_uploader :file, PhotoUploader

  validate :name, presence: true
  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  before_validation do
    if self.new_record? and self.uuid.blank?
      write_attribute(:uuid, SecureRandom.uuid) 
    end
    true
  end
end

# == Schema Information
#
# Table name: photos
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  desc       :string(255)
#  uuid       :string(255)      not null
#  album_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  file       :string(255)      default("")
#  link       :string(255)
#  horizental :boolean          default(TRUE)
#

