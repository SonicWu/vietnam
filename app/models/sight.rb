# -*- encoding : utf-8 -*-
class Sight < ActiveRecord::Base
  attr_accessible :city_id, :content, :desc, :name, :uuid, :cover, :category

  CATEGORIES = %w(
    普通景点
    旅游婚纱 
    奥黛定做
  )

  default_scope where(category: 0)
  scope :random, order("RAND()")

  has_one :album
  belongs_to :city
  has_many :schemesights
  has_many :schemes, :through => :schemesights

  mount_uploader :cover, PhotoUploader

  validate :name, presence: true
  # validate :cover, presence: true
  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  before_validation do
    if self.uuid.blank? # and self.new_record?
      write_attribute(:uuid, SecureRandom.uuid) 
    end
    true
  end
end

# == Schema Information
#
# Table name: sights
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  desc       :string(255)      default("")
#  content    :text
#  city_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  cover      :string(255)      not null
#  uuid       :string(255)      not null
#

