class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= AdminUser.new

    if user.superuser?
      can :manage, :all
    elsif user.admin?
      can :manage, :all
    elsif user.receiver?
      can [:read, :create, :new_pickup, :new_tour], Order
      can :update, Order, :status => [1,2,4,5]
      can :manage, Flight
      can :manage, Contact
      can :manage, Hotel
    elsif user.agency?
      can :read, Order
      can :update, Order, :status => [2,3]
      can :settle, Order
      can :read, Flight
      can :read, Contact
      can :read, Hotel
    elsif user.receiver_manager?
      can :manage, Order
      can :manage, Flight
      can :manage, Contact
      can :manage, Hotel
    end
  end
end
