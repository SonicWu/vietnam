# -*- encoding : utf-8 -*-
class TravelAgency < ActiveRecord::Base
  attr_accessible :contact_email, :contact_name, :contact_phone, :contact_qq, :name

  has_many :schemes

  validates :name, presence: true
end

# == Schema Information
#
# Table name: travel_agencies
#
#  id            :integer          not null, primary key
#  name          :string(255)      not null
#  contact_name  :string(255)
#  contact_phone :string(255)
#  contact_qq    :string(255)
#  contact_email :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

