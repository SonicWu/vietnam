# -*- encoding : utf-8 -*-
class City < ActiveRecord::Base
  attr_accessible :name, :woeid, :desc, :area_id,
                  :content, :cover, :uuid, :prior, :has_experts, :has_homestays,
                  :route_info, :hotel_info, :traffic_info, :map

  belongs_to :area
  has_many :sights
  has_many :experts
  has_many :homestays

  mount_uploader :cover, PhotoUploader
  # mount_uploader :map, CityMapUploader

  validates :area_id, presence: true
  validates :name, presence: true
  #validates :cover, presence: true
  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  default_scope order('prior DESC')

  scope :has_experts, where(has_experts: true)
  scope :has_homestays, where(has_homestays: true)

  before_validation do
    if self.uuid.blank? # and self.new_record?
      write_attribute(:uuid, SecureRandom.uuid)
    end
    true
  end
end

# == Schema Information
#
# Table name: cities
#
#  id           :integer          not null, primary key
#  name         :string(255)
#  desc         :text
#  woeid        :integer
#  area_id      :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  content      :text
#  cover        :string(255)      not null
#  uuid         :string(255)      not null
#  weather      :text
#  route_info   :text
#  hotel_info   :text
#  traffic_info :text
#  map          :string(255)
#

