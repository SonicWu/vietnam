# -*- encoding : utf-8 -*-
class Info < ActiveRecord::Base
  attr_accessible :cover, :name, :uuid, :link, :category, :prior

  has_many :info_items

  mount_uploader :cover, InfoUploader

  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  before_validation do
    if self.uuid.blank? # and self.new_record?
      write_attribute(:uuid, SecureRandom.uuid)
    end
    true
  end

  CATEGORIES = %w(
    旅游地图
    旅游攻略
    订票教程
  )

  default_scope order('prior DESC')
  scope :maps, where(category: 0)
  scope :strategies, where(category: 1)
  scope :tickets, where(category: 2)
end
