# -*- encoding : utf-8 -*-

class Expert < ActiveRecord::Base
  attr_accessible :name, :uuid, :desc, :city_id,
                  :intro, :cover, :contact

  mount_uploader :cover, AvatarUploader

  belongs_to :city

  validates :city_id, presence: true
  validates :name, presence: true
  validates :cover, presence: true
  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  before_validation do
    if self.uuid.blank? # and self.new_record?
      write_attribute(:uuid, SecureRandom.uuid)
    end
    true
  end
end
