# -*- encoding : utf-8 -*-
class Settle < ActiveRecord::Base
  attr_accessible :date, :scheme_id
  belongs_to :scheme
end
