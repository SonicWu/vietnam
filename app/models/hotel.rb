class Hotel < ActiveRecord::Base
  attr_accessible :name, :address, :phone, :order_id, :flight_id

  validates :name, :address, presence: true
end

# == Schema Information
#
# Table name: hotels
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  address    :string(255)      not null
#  order_id   :integer
#  flight_id  :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  phone      :string(255)      not null
#

