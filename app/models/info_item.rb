# -*- encoding : utf-8 -*-
class InfoItem < ActiveRecord::Base
  attr_accessible :cover, :name, :uuid, :link, :info_id, :prior

  # mount_uploader :cover, InfoUploader

  validates :uuid, :presence => true, :uniqueness => { case_sensitive: false }

  before_validation do
    if self.uuid.blank? # and self.new_record?
      write_attribute(:uuid, SecureRandom.uuid)
    end
    true
  end

  default_scope order('prior DESC')
end
