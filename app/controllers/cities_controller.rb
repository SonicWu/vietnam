# -*- encoding : utf-8 -*-
class CitiesController < ApplicationController
  def show
    @city = City.find(params[:id])
    @area = @city.area
    @cities = @area.cities
    @areas = Area.all
    @sights = @city.sights
  end
end
