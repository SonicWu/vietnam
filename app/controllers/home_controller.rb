# -*- encoding : utf-8 -*-
class HomeController < ApplicationController
  def index
    @areas = Area.all
    @hot_schemes = Scheme.random[0...4]
    @photos = Photo.horizental.order("RAND()").select {|p| p.album.pictures? }[0...8]
    # @photos = Photo.horizental.select {|p| p.album.pictures? }[0...8]
    @guides = Guide.hot
    @slide_photo = Album.slide.photos.order("RAND()").first
  end

  def about_us
  end

  def contact_us
  end

  def wedding
    @sight = Sight.where(category: 1).first
    if @sight.album
      @gallery_photos = @sight.album.photos
    else
      @gallery_photos = []
    end
  end

  def tailor
    @sight = Sight.where(category: 2).first
    if @sight.album
      @gallery_photos = @sight.album.photos
    else
      @gallery_photos = []
    end
  end

  def girls
    @albums = Album.where(category: 4).order('created_at DESC').page(params[:page]).per(10)
  end

  def girl
    @girl_album = Album.find(params[:id])
  end

  def charter
    @album = Album.where(category: 5).order('created_at DESC').first
    @guides = Guide.charters
  end
end
