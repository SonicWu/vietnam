class GuidesController < ApplicationController
  def index
    @guides = []
    @guide_nav = Guide::CATEGORIES[0..3]

    @guide_nav.each_with_index do |item, index|
      @guides << {
        name: item,
        guides: Guide.where(category: index)
      }
    end
  end
end
