class HomestaysController < ApplicationController
  def index
    @cities = City.has_homestays
    @homestay_intro = Site.first.homestays_intro
  end

  def show
    @homestay = Homestay.find(params[:id])
    @city = @homestay.city
  end
end
