# -*- encoding : utf-8 -*-
class ExpertsController < ApplicationController
  def index
    @cities = City.has_experts
    @expert_intro = Site.first.experts_intro
  end

  def show
    @expert = Expert.find(params[:id])
    @city = @expert.city
  end
end
