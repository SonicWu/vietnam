# -*- encoding : utf-8 -*-
class OrdersController < ApplicationController
  load_and_authorize_resource
  layout "admin"

  before_filter :is_signin

  def index
    @category = (params[:category] || -1).to_i

    if @category < 0
      redirect_to orders_path(category: 0)
      return
    end

    @status = (params[:condition_status] || -1).to_i
    @contact = params[:condition_contact]
    @scheme_id = (params[:condition_scheme] || -1).to_i
    @area_id = (params[:condition_area] || -1).to_i
    @date = params[:condition_date]

    if current_admin_user.receiver?
      @orders = Order.where(status: [1,2,3,4,5,6])
    elsif current_admin_user.agency?
      @orders = Order.where(status: [2,3,4,5,6])
    elsif current_admin_user.admin? or current_admin_user.superuser? or current_admin_user.receiver_manager?
      @orders = Order.where(status: [1,2,3,4,5,6])
    end

    if @category > -1
      @orders = @orders.where(category: @category)
    end

    if @status > -1
      @orders = @orders.where(status: @status)
    end

    if @scheme_id > -1
      @orders = @orders.where(scheme_id: @scheme_id)
    end

    if (@date.nil? or @date.empty?) and (@contact.nil? or @contact.empty?) and (current_admin_user.receiver? or current_admin_user.agency?)
      d = Date.today

      if @category == 0
        time_range = d...(d+3.day)
        @orders = @orders.where(tour_time: time_range)
      elsif @category == 1
        orders = []
        @orders.each do |order|
          if not order.flight.nil? and order.flight.arrive_at >= d and order.flight.arrive_at < (d + 1.day)
            orders.push(order)
          end
        end 

        @orders = orders
      end
    end

    if not @contact.nil? and not @contact.empty?
      q = "%#{ @contact }%"
      @orders = @orders.joins(:contacts).where('name LIKE ? OR chinese_phone LIKE ?', q, q)
    end

    if not @date.nil? and not @date.empty?
      @date = @date.to_date

      if @category == 0
        time_range = @date...(@date+1.day)
        @orders = @orders.where(tour_time: time_range)
      elsif @category == 1
        orders = []
        @orders.each do |order|
          if not order.flight.nil? and order.flight.arrive_at >= @date and order.flight.arrive_at < (@date + 1.day)
            orders.push(order)
          end
        end 

        @orders = orders
      end
    end

    if @area_id > -1
      orders = []
      @orders.each do |order|
        if not order.scheme.nil? and order.scheme.area_id.to_i == @area_id
          orders.push(order)
        end
      end

      @orders = orders
    end
  end

  def show
    @order = Order.find(params[:id])
    @scheme = @order.scheme
    @flight = @order.flight
    @hotel = @order.hotel
  end

  def new
  end

  def new_pickup
    if current_admin_user.receiver?
      @available_status = [1,2]
    elsif current_admin_user.agency?
      @available_status = [3,4,5]
    elsif current_admin_user.admin? or current_admin_user.superuser? or current_admin_user.receiver_manager?
      @available_status = [1,2,3,4,5]
    end
  end

  def new_tour
    if current_admin_user.receiver?
      @available_status = [1,2]
    elsif current_admin_user.agency?
      @available_status = [3,4,5]
    elsif current_admin_user.superuser? or current_admin_user.admin? or current_admin_user.receiver_manager?
      @available_status = [1,2,3,4,5]
    end

  end

  def create
    category = params[:category].to_i
    status = params[:status].to_i

    tour_children_free = params[:tour_children_free].to_i
    tour_children_discount = params[:tour_children_discount].to_i
    tour_adult_count = params[:tour_adult_count].to_i
    tour_members_count = tour_children_free + tour_children_discount + tour_adult_count

    first_contact_name = params[:first_contact_name]
    first_contact_gender = params[:first_contact_gender]
    first_contact_chinese_phone = params[:first_contact_chinese_phone]
    first_contact_vietnamese_phone = params[:first_contact_vietnamese_phone]
    first_contact_passport_number = params[:first_contact_passport_number]
    first_contact_qq = params[:first_contact_qq]
    first_contact_wechat = params[:first_contact_wechat]
    first_contact_weibo = params[:first_contact_weibo]
    first_contact_email = params[:first_contact_email]

    second_contact_name = params[:second_contact_name]
    second_contact_gender = params[:second_contact_gender]
    second_contact_chinese_phone = params[:second_contact_chinese_phone]
    second_contact_vietnamese_phone = params[:second_contact_vietnamese_phone]
    second_contact_passport_number = params[:second_contact_passport_number]
    second_contact_qq = params[:second_contact_qq]
    second_contact_wechat = params[:second_contact_wechat]
    second_contact_weibo = params[:second_contact_weibo]
    second_contact_email = params[:second_contact_email]

    if category == 0
      scheme = params[:scheme]
      tour_time = params[:tour_time].to_datetime
      order = Order.create!(scheme_id: scheme,
                            tour_time: tour_time,
                            tour_members_count: tour_members_count,
                            tour_children_free: tour_children_free,
                            tour_children_discount: tour_children_discount,
                            tour_adult_count: tour_adult_count,
                            status: status,
                            category: category)
    elsif category == 1
      flight_number = params[:flight_number]
      flight_airline = params[:flight_airline]
      flight_arrive_at = params[:flight_arrive_at].to_datetime
      
      order = Order.create!(tour_members_count: tour_members_count,
                            tour_children_free: tour_children_free,
                            tour_children_discount: tour_children_discount,
                            tour_adult_count: tour_adult_count,
                            status: status,
                            category: category)

      flight = Flight.create!(number: flight_number,
                              airline: flight_airline,
                              arrive_at: flight_arrive_at,
                              order_id: order.id)
    end

    first_contact = Contact.create!(name: first_contact_name,
                                    gender: first_contact_gender,
                                    chinese_phone: first_contact_chinese_phone,
                                    vietnamese_phone: first_contact_vietnamese_phone,
                                    passport_number: first_contact_passport_number,
                                    qq: first_contact_qq,
                                    wechat: first_contact_wechat,
                                    weibo: first_contact_weibo,
                                    email: first_contact_email,
                                    order_id: order.id)

    if not second_contact_name.blank? and not second_contact_chinese_phone.blank?
      second_contact = Contact.create!(name: second_contact_name,
                                       gender: second_contact_gender,
                                      chinese_phone: second_contact_chinese_phone,
                                      vietnamese_phone: second_contact_vietnamese_phone,
                                      passport_number: second_contact_passport_number,
                                      qq: second_contact_qq,
                                      wechat: second_contact_wechat,
                                      weibo: second_contact_weibo,
                                      email: second_contact_email,
                                      order_id: order.id)
    end

    hotel_name = params[:hotel_name]
    hotel_address = params[:hotel_address]
    hotel_phone = params[:hotel_phone]

    hotel = Hotel.create!(name: hotel_name,
                          address: hotel_address,
                          phone: hotel_phone,
                          order_id: order.id)

    render json: {
      url: order_path(order)
    }
  end

  def edit
    @order = Order.find(params[:id])
    contacts = @order.contacts
    @first_contact = contacts.first
    @second_contact = contacts.second
    @hotel = @order.hotel

    if current_admin_user.receiver?
      if @order.status == 4
        @available_status = [4,6]
      elsif @order.status == 5
        @available_status = [5,6]
      else
        @available_status = [1,2,4]
      end
    elsif current_admin_user.agency?
      @available_status = [3,4,5]
    elsif current_admin_user.superuser? or current_admin_user.admin? or current_admin_user.receiver_manager?
      @available_status = [1,2,3,4,5,6]
    end

    if @order.tour?
      render 'edit_tour'
    elsif @order.pickup?
      @flight = @order.flight
      render 'edit_pickup'
    end
  end

  def update
    @order = Order.find(params[:id])
    status = params[:status].to_i

    if (@order.status == 4 or @order.status == 5) and current_admin_user.receiver?
      @order.status = status
      @order.save!

      render json: {
        url: order_path(@order.id)
      }
      return
    end

    tour_children_free = params[:tour_children_free].to_i
    tour_children_discount = params[:tour_children_discount].to_i
    tour_adult_count = params[:tour_adult_count].to_i
    tour_members_count = tour_children_free + tour_children_discount + tour_adult_count

    first_contact_name = params[:first_contact_name]
    first_contact_gender = params[:first_contact_gender]
    first_contact_chinese_phone = params[:first_contact_chinese_phone]
    first_contact_vietnamese_phone = params[:first_contact_vietnamese_phone]
    first_contact_passport_number = params[:first_contact_passport_number]
    first_contact_qq = params[:first_contact_qq]
    first_contact_wechat = params[:first_contact_wechat]
    first_contact_weibo = params[:first_contact_weibo]
    first_contact_email = params[:first_contact_email]

    second_contact_name = params[:second_contact_name]
    second_contact_gender = params[:second_contact_gender]
    second_contact_chinese_phone = params[:second_contact_chinese_phone]
    second_contact_vietnamese_phone = params[:second_contact_vietnamese_phone]
    second_contact_passport_number = params[:second_contact_passport_number]
    second_contact_qq = params[:second_contact_qq]
    second_contact_wechat = params[:second_contact_wechat]
    second_contact_weibo = params[:second_contact_weibo]
    second_contact_email = params[:second_contact_email]

    if @order.tour?
      scheme = params[:scheme]
      tour_time = params[:tour_time].to_datetime
      order = @order.update_attributes!(scheme_id: scheme,
                            tour_time: tour_time,
                            tour_members_count: tour_members_count,
                            tour_children_free: tour_children_free,
                            tour_children_discount: tour_children_discount,
                            tour_adult_count: tour_adult_count,
                            status: status)

    elsif @order.pickup?
      flight = @order.flight
      flight.destroy unless flight.nil?

      flight_number = params[:flight_number]
      flight_airline = params[:flight_airline]
      flight_arrive_at = params[:flight_arrive_at].to_datetime
      
      order = @order.update_attributes!(tour_members_count: tour_members_count,
                            tour_children_free: tour_children_free,
                            tour_children_discount: tour_children_discount,
                            tour_adult_count: tour_adult_count,
                            status: status)

      flight = Flight.create!(number: flight_number,
                              airline: flight_airline,
                              arrive_at: flight_arrive_at,
                              order_id: @order.id)
    end

    if status == 4
      order = @order.update_attributes!(cancel_reason: params[:cancel_reason])
    end

    @order.contacts.destroy_all

    first_contact = Contact.create!(name: first_contact_name,
                                    gender: first_contact_gender,
                                    chinese_phone: first_contact_chinese_phone,
                                    vietnamese_phone: first_contact_vietnamese_phone,
                                    passport_number: first_contact_passport_number,
                                    qq: first_contact_qq,
                                    wechat: first_contact_wechat,
                                    weibo: first_contact_weibo,
                                    email: first_contact_email,
                                    order_id: @order.id)

    if not second_contact_name.blank? and not second_contact_chinese_phone.blank?
      second_contact = Contact.create!(name: second_contact_name,
                                      gender: second_contact_gender,
                                      chinese_phone: second_contact_chinese_phone,
                                      vietnamese_phone: second_contact_vietnamese_phone,
                                      passport_number: second_contact_passport_number,
                                      qq: second_contact_qq,
                                      wechat: second_contact_wechat,
                                      weibo: second_contact_weibo,
                                      email: second_contact_email,
                                      order_id: @order.id)
    end

    hotel_name = params[:hotel_name]
    hotel_address = params[:hotel_address]
    hotel_phone = params[:hotel_phone]

    if not hotel_name.empty? and not hotel_address.empty?
      hotel = @order.hotel
      hotel.destroy unless hotel.nil?

      hotel = Hotel.create!(name: hotel_name,
                            address: hotel_address,
                            phone: hotel_phone,
                            order_id: @order.id)
    end

    render json: {
      url: order_path(@order.id)
    }
  end

  def destroy
    order = Order.find(params[:id])
    order.destroy

    render json: {success: true}
  end

  def settle 
    @items = []
    sd = Date.today
    ed = sd - 30.day

    sd.downto(ed) do |d|
      orders = Order.where(tour_time: d, status: 6)

      if orders.count > 0
        Scheme.all.each do |s|
          scheme_orders = orders.where(scheme_id: s.id)

          if scheme_orders.count > 0
            settle = Settle.where(date: d, scheme_id: s.id)
            tourist = 0

            scheme_orders.each do |order|
              tourist += order.tour_adult_count + order.tour_children_discount * 0.5
            end

            @items.push({date: d, scheme: s, settled: settle.count > 0, tourist: tourist})
          end
        end
      end
    end
  end

  def set_settle
    date = params[:date]
    scheme_id = params[:scheme_id]

    Settle.create!(date: date, scheme_id: scheme_id)
    render json: {success: true}
  end

  private
  def is_signin
    if current_admin_user.nil?
      current_admin_user = nil

      if request.xhr?
        render json: {
          errors: [{ msg: '您尚未登录或者登录已经超时。请重新登录。' }]
        }, status: 403
      else
        redirect_to new_admin_user_session_path
      end
    end
  end
end
