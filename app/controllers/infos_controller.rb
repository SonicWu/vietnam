# -*- encoding : utf-8 -*-
class InfosController < ApplicationController
  def index
    @maps = Info.maps
    @strategies = Info.strategies
    @tickets = Info.tickets
    @guides = Guide.infos
  end

  def show
    @info = Info.find(params[:id])
    @items = @info.info_items
  end
end
