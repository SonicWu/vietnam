# -*- encoding : utf-8 -*-
class AlbumsController < ApplicationController
  def show
    @album = Album.find(params[:id])
  end
end
