# -*- encoding : utf-8 -*-
class SightsController < ApplicationController
  def show
    @sight = Sight.find(params[:id])
    @city = @sight.city
    @area = @city.area
    @cities = @area.cities
    @areas = Area.all


    if @sight.album
      @gallery_photos = @sight.album.photos
    else
      @gallery_photos = []
    end
  end
end
