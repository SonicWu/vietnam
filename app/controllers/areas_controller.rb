# -*- encoding : utf-8 -*-
class AreasController < ApplicationController
  def show

    @areas = Area.all
    @area = Area.find(params[:id])
    @cities = @area.cities

    # redirect_to city_path(@cities.first)
    # return

    @schemes = @area.schemes
    # client = Weatherman::Client.new
    # @weather = client.lookup_by_woeid @area.woeid
  end
end
