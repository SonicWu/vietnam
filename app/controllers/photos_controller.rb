class PhotosController < ApplicationController
  def show

    @photo = Photo.find(params[:id])
    @album = @photo.album
    @photos = @album.photos
    @lists = AlbumList.all
  end
end
