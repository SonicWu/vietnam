# -*- encoding : utf-8 -*-
module ApplicationHelper
  def nav_link_to(link_text, link_path)
    class_name = ''

    if (['areas', 'schemes'].include? params[:controller] \
        and /areas|schemes/ =~ link_path) \
      or (['cities', 'sights'].include? params[:controller] \
        and /cities|sights/ =~ link_path) \
      or current_page?(link_path)

      class_name = 'active'
    end

    content_tag(:li, :class => class_name) do
      link_to link_text, link_path
    end
  end

  def scheme_nav_path
    area = case params[:controller]
           when 'schemes' then @scheme.area
           when 'cities' then @city.area
           when 'sights' then @sight.city.area
           when 'areas' then @area
           else Area.first
           end

    area_path(area)
  end

  def sight_nav_path
    city = case params[:controller]
           when 'sights' then @sight.city
           when 'cities' then @city
           when 'schemes' then @scheme.area.cities.first
           when 'areas' then @area.cities.first
           else City.first
           end

    city_path(city)
  end

  def name_by_lang(model, lang=:zh_cn)
    names = model.name.split('-')
    return names[0].strip
  end

  def fieldset_disable?
    (@order.status == 4 or @order.status == 5) and current_admin_user.receiver?
  end

end
