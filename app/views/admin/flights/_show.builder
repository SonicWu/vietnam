# -*- encoding : utf-8 -*-
context.instance_eval  do
  panel "航班信息" do
    attributes_table_for(flight) do
      row "航班号" do
        flight.number
      end
      row "航空公司" do
        flight.airline
      end
      row "何时抵达" do
        flight.arrive_at
      end
    end
    # render "admin/hotels/show", hotel: flight.hotel, title: nil, context: self if flight.hotel
  end
end
