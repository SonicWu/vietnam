# -*- encoding : utf-8 -*-
context.instance_eval do
  panel "订单基本信息" do
    attributes_table do
      row "订单类型" do
        Order::CATEGORIES[order.category]
      end
      row "旅游线路" do
        order.scheme
      end
      row "旅游日期" do
        order.tour_time
      end
      row "旅游人数" do
        order.tour_members_count
      end
      row "免费儿童数" do
        order.tour_children_free
      end
      row "半价儿童数" do
        order.tour_children_discount
      end
      row "成人数" do
        order.tour_adult_count
      end
    end
  end
  render "admin/contacts/index", contacts: order.contacts, context: self
  render "admin/hotels/show", hotel: order.hotel, title: "酒店信息", context: self if order.hotel
  render "admin/flights/show", flight: order.flight, context: self if order.flight
  active_admin_comments
end
