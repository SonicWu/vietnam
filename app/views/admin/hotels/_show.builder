# -*- encoding : utf-8 -*-
context.instance_eval  do
  panel title do
    attributes_table_for(hotel) do
      row "酒店名称" do
        hotel.name
      end
      row "酒店地址" do
        hotel.address
      end
      row "酒店电话" do
        hotel.phone
      end
    end
  end
end
