# -*- encoding : utf-8 -*-
context.instance_eval  do
  panel "联系人信息" do
    table_for(contacts, :sortable => true, :class => 'index_table') do
      column "姓名", :name
      column "中国手机", :chinese_phone
      column "越南手机", :vietnamese_phone
      column "护照号", :passport_number
      column "QQ", :qq
      column "微信", :wechat
      column "微博", :weibo
      column "邮箱", :email
      default_actions rescue nil # test for responds_to? does not work.
    end
  end
end
