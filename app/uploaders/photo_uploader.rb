# -*- encoding : utf-8 -*-

class PhotoUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  # include Sprockets::Helpers::RailsHelper
  # include Sprockets::Helpers::IsolatedHelper

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  process :resize_to_limit => [960, 10000]

  version :pic do
    process :resize_to_fit => [nil, 440]
    process :quality => 65
    process :store_horizental, :if => :pic?
  end

  version :pic_with_watermark, :from_version => :pic do
    process :watermark
  end

  version :pic_thumb, :from_version => :pic do
    process :resize_to_fill => [80, 80]
    process :quality => 80
  end

  version :thumb do
    process :resize_to_fit => [300, nil]
    process :quality => 75
  end

  def watermark
    manipulate! do |img|
      logo = MiniMagick::Image.from_file("#{Rails.root}/app/assets/images/watermark.png")
      img = img.composite(logo) do |c|
       c.gravity 'SouthWest' #, 15, 0, Magick::OverCompositeOp)
       c.geometry '+30+20'
      end
    end
  end

  def store_horizental
    width, height = `identify -format "%wx%h" #{file.path}`.split(/x/)
    if model
      model.horizental = width > height
    end
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  def extension_white_list
     %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    # "#{model.uuid}_#{model.name}.#{file.extension}"
    "#{model.uuid}.#{file.extension}"
  end

  protected
    def pic?(file)
      model.is_a?Photo and model.album and (model.album.pictures? or model.album.sights?)
    end
end
