$ ->
  $(document)
  .on('ajax:success', '.link-destroy-order', () ->
    $(this).closest('tr').fadeOut(200, () ->
      $(this).remove()
    )
  )

  $('[data-behaviour~=datepicker]').datetimepicker({
    language: 'zh-CN'
    pickTime: false
  })
