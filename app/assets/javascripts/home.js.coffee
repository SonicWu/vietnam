#= require slides.jquery

$(window).on('load', ->
  $('#slides').slides
    container: 'slides-list'
    preload: true
    preloadImage: 'img/loading.gif'
    play: 5000
    pause: 2500
    hoverPause: true
    animationStart: (current) ->
      $('.caption').animate({
        bottom:-35
      },100)

      if window.console and console.log
        # example return of current slide number
        console.log('animationStart on slide: ', current)
    animationComplete: (current) ->
      $('.caption').animate({
        bottom:0
      },200)

      if window.console and console.log
        # example return of current slide number
        console.log('animationComplete on slide: ', current)
    slidesLoaded: ->
      $('.caption').animate({
        bottom:0
      },200)
)
