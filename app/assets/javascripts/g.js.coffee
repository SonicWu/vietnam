$ ->
  # setFixedContact()

  $('body').on('mousedown', 'img', (e) ->
    if e.which == 3
      e.stopPropagation()
      e.preventDefault()
      return false
  )

# 如果浏览器宽度不够就不要显示position fixed的联系信息
setFixedContact = () ->
  container = $('.container')
  contact = $('.fixed-contact')

  width = container.outerWidth() + 2*(10 + contact.outerWidth())
  winWidth = $(window).width()

  if (width > winWidth)
    contact.hide()
