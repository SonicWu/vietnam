$ ->
  $('[data-behaviour~=datetimepicker]').datetimepicker({
    language: 'zh-CN'
    maskInput: true
    # startDate: moment().startOf('day').add('day', 1).toDate()
    startDate: new Date()
  })

  $('[data-behaviour~=datepicker]').datetimepicker({
    language: 'zh-CN'
    pickTime: false
    maskInput: true
    # startDate: moment().startOf('day').add('day', 1).toDate()
    startDate: new Date()
  })

  # $('input[type=checkbox]').on('click', (e) ->
  #   chb = $(e.currentTarget)
  #   checked = chb.is(':checked')
  #   fieldset = chb.closest('fieldset')
  #   groups = fieldset.find('.control-groups')
  #   formItems = groups.find('input')

  #   if checked
  #     formItems.prop('disabled', false)
  #     groups.slideDown()
  #   else
  #     groups.slideUp(->
  #       formItems.prop('disabled', true)
  #     )
  # )

  $('.form').on('ajax:success', (e, result) ->
    location.href = result.url
  )

  $('.form-edit').on('ajax:beforeSend', (e) ->
    changed = false

    $('#tour-adult-count, #tour-children-free, #tour-children-discount').each(() ->
      if $(this).data('origin')*1 != this.value*1
        changed = true
        return false
    )

    if changed
      unless window.confirm('成人数 / 免费儿童数 / 半价儿童数 发生变化，确定要保存？')
        return false
  )

  $('select#status').change((e) ->
    setReason()
  )

  setReason()

setReason = () ->
  select = $('select#status')
  reasonEl = $('#reason')

  if select.val()*1 == 4
      reasonEl.show()
    else
      reasonEl.hide()
