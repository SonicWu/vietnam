@Sonic =


  loadImage: (file, callback) ->
    if callback and not $.isFunction(callback)
      console.log('callback is no a valid FUNCTION')
      return

    img = new Image()

    if callback
      img.onload = () ->
        callback(img)

      img.onerror = () ->
        callback()

    if typeof file == "string"
      img.src = file
    else if file.nodeName && file.nodeName == "IMG"
      img.src = file.src
    else if window.FileReader && FileReader.prototype.readAsDataURL
      fileReader = new FileReader()
      fileReader.onload = (e) ->
        img.src = e.target.result
      fileReader.readAsDataURL(file)
    else
      callback()



  preloadImages: (images) ->
    $.each(images, (idx, img) ->
      Sonic.loadImage(img)
    )



  scrollTo: (target, opts) ->
    opts = $.extend({
      anim: true
      container: null
    }, opts)

    offset = null
    if opts.container
      container = $(opts.container)
    else
      container = $('html, body')

    if typeof target == "object" and (typeof target.top == "number" or typeof target.left == "number" )
      offset = target
    else
      targetOffset = $( target ).offset()
      containerOffset = container.offset() || { top: 0, left: 0 }

      offset =
        top: targetOffset.top - containerOffset.top - 30
        left: targetOffset.left - containerOffset.left - 30

    if opts.anim
      container.animate({
        scrollTop: offset.top
        scrollLeft: offset.left
      }, 500)
    else
      container.scrollTop(offset.top)
        .scrollLeft(offset.left)



  validateField: (input, blur) ->
    rules = input.attr( "data-validate" )
    msgs = input.attr( "data-validate-msg" )
    val = input.val()
    valid = true
    error = ""

    if rules
      rules = rules.split( ";" )
    else
      return valid

    if msgs != undefined
      msgs = msgs.split( ";" )

    $.each( rules, ( i, rule ) ->
      rule = $.trim(rule).split(':')
      type = rule[0]
      args = rule[1] || null
      result = null

      if type == "custom"
        event = $.Event( "validate" )
        input.trigger( event, [val, args] )
        result = event.result || { valid: true }
      else
        result = Sonic.validate[type]( val, args )

      if !result.valid
        valid = result.valid

        if msgs && msgs.length && msgs[i]
          error = msgs[i]
        else if result.errorMsg
          error = result.errorMsg
        else
          error = ''

        if type == 'required' && blur
          valid = true
          error = ''

        return false
    )

    if input.parent().is('.input-append')
      inputWrap = input.closest('.input-append')
    else
      inputWrap = input

    if !valid
      input.addClass( "error" )
      inputWrap.nextAll( ".desc" )
        .hide()

      if error
        errorEl = inputWrap.nextAll( ".error" )

        if !errorEl.length
          errorEl = $( "<p class='error'></p>" ).insertAfter(inputWrap)

        errorEl.text( error )
          .show()
    else
      input.removeClass( "error" )
      inputWrap.nextAll( ".desc" ).show()
      inputWrap.nextAll( ".error" ).remove()

    return valid



  validate:
    required: (val, args) ->
      return {
        valid: !!$.trim( val ),
        errorMsg: "请填写这个字段"
      }

    length: ( val, args ) ->
      args = ( args || "0" ).split( "," )
      len = val.length
      min = args[0] * 1
      max = args.length > 1 ? args[1] * 1 : null

      return {
        valid: !val || ( len >= min && ( !max || len <= max )),
        errorMsg: "这个字段的长度不能" + ( min > 0 ? "少于" + min + "位" : "" ) + ( max ? "多余" + max + "位" : "" )
      }

    range: ( val, args ) ->
      args = ( args || "0" ).split( "," )
      min = args[0] * 1
      max = args.length > 1 ? args[1] * 1 : null

      return {
        valid: !val || ( val >= min && ( !max || val <= max )),
        errorMsg: "这个字段的值" + ( min > 0 ? "不能小于" + min : "" ) + ( max ? "不能大余" + max : "" )
      }

    email: ( val, args ) ->
      re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return {
        valid: !val || re.test( val ),
        errorMsg: "请填写正确的邮箱地址"
      }

    number: ( val, args ) ->
      return {
          valid: !val || /^\d+$/.test( val ),
          errorMsg: "请填写数字"
      }

    mobile: ( val, args ) ->
      return {
        valid: !val || /^\d{11}$/.test( val ),
        errorMsg: "请填写一个有效的手机号码"
      }

    phone: (val, args) ->
      return {
        # include +86-12345678 400-800-2322 18615706914 86-18615706914
        valid: !val || /^(\+)?[0-9]+(-[0-9]+)*$/.test(val),
        errorMsg: "请填写一个有效的联系电话"
      }
