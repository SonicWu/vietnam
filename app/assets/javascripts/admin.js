//= require jquery
//= require jquery_ujs
//= require sonic
//= require moment
//= require bootstrap-tab
//= require ckeditor/init
//= require bootstrap-datetimepicker
//= // require locales/bootstrap-datetimepicker.zh-CN

$.fn.datetimepicker.defaults = {
	language: 'zh-CN',
	maskInput: true,           // disables the text input mask
	pickDate: true,            // disables the date picker
	pickTime: true,            // disables de time picker
	pick12HourFormat: false,   // enables the 12-hour format time picker
	pickSeconds: true,         // disables seconds in the time picker
	startDate: -Infinity,      // set a minimum date
	endDate: Infinity          // set a maximum date
};

$(function() {
	// trigger before ujs form submit event
	if ( $.rails ) {
		$( "body" )
			.on( "keydown", "form.form[data-remote=true] textarea", function( e ) {
				var metaKey = mcw.metaKey( e );
				if ( metaKey && e.which == 13 ) {
					e.preventDefault();
					$( this ).closest( "form.form" ).submit();
				}
			})
			.on ( "submit", "form.form", function( e ) {
				var form = $( e.currentTarget ),
					valid = true,
					selector = [
						"input[data-validate]:visible",
						"textarea[data-validate]:visible"
					].join(',');

				form.find(selector).each( function( i, input ) {
					if ( !Sonic.validateField( $( input ))) {
						valid = false;
					}
				});

				return valid;
			})
			.on( "blur.validate", "input[data-validate]", function( e ) {
				var input = $( this );
				if ( input.attr( "data-blur-validate" ) == "true" ) {
					Sonic.validateField( input, true );
				}
			})
			.on( "ajax:error", "form.form[data-remote=true]", function( e, xhr ) {
		        var form = $( e.currentTarget ),
					errors = $.parseJSON( xhr.responseText ).errors;
		        $.each( errors, function( i, err ) {
					var field = form.find( "input[name=" + err.target + "], textarea[name=" + err.target + "], select[name=" + err.target + "]" ),
						errorEl = field.nextAll( ".error" );

					if ( !errorEl.length ) {
						errorEl = $( "<p class='error'></p>" ).insertAfter( field );
						errorEl.parent(".mcw-editor").addClass("error");
					}

					errorEl.text( err.msg );
					field.addClass( "error" )
						.nextAll( ".desc" ).hide();
		        });
			})
			.on( "ajax:beforeSend", "form.form[data-remote=true]", function( e, xhr, settings ) {
				var btn = $(this).find('button[type=submit]');
				if ( btn.is( "[data-global-loading]" )) {
					mcw.globalLoading( btn.data( "global-loading" ));
				} else if ( btn.is( "[data-loading]" )) {
					mcw.tinyLoading( btn );
				}
			})
			.on( "ajax:complete", "form.form[data-remote=true]", function( e, xhr, status ) {
				var btn = $(this).find('button[type=submit]');

				if ( btn.is( "[data-global-loading]" )) {
					mcw.globalLoading( "hide" );
				} else if ( btn.is( "[data-loading]" )) {
					mcw.tinyLoading( link, false );
				}

				if ( status == "success" ) {
					var button = $( this ).find( $.rails.enableSelector ),
						successText = button.attr( "data-success-text" ),
						result = $.parseJSON( xhr.responseText ),
						gotoUrl = result && result.target_url || button.attr( "data-goto" ),
						isRefresh = button.data( "refresh" );

					if ( successText ) {
						button.text( successText + " \u2713" )
							.prop( "disabled", true )
							.addClass( "success" );
					}

					if ( gotoUrl ) {
						var url = mcw.urlParts( gotoUrl ),
							currentUrl = mcw.urlParts( location.href );

						if ( url.path != currentUrl.path ) {
							if ( $( ".workspace" ).length ) {
								mcw.stack({
									url: gotoUrl,
									replace: true,
									root: button.is('[data-goto-root]'),
									restorePosition: true
								});
							} else {
								location.href = gotoUrl;
							}
						}
					}

					if ( isRefresh ) {
						mcw.stack({
							url: location.href,
							nocache: true
						});
					}

					if (successText && !gotoUrl && !isRefresh) {
					    setTimeout(function() {
					        button.text( button.data('ujs:enable-with') )
					            .prop('disabled', false)
					            .removeClass('success');
					    }, 3000);
					}

					if (successText || gotoUrl || isRefresh) {
					    return false;
					}
				}
			})
			.on( "ajax:complete", $.rails.linkClickSelector, function( e, xhr, status ) {
				var link = $( this );


				if ( link.is( "[data-global-loading]" )) {
					mcw.globalLoading( "hide" );
				} else if ( link.is( "[data-loading]" )) {
					mcw.tinyLoading( link, false );
				}

				if ( link.is('.btn[data-disable-with]')) {
					link.text(link.data('ujs:enable-with'))
						.prop('disabled', false);
				}

				if ( status == "success" ) {
					var result = $.parseJSON( xhr.responseText ),
						gotoUrl = result && result.target_url || link.attr( "data-goto" ),
						isRefresh = link.data( "refresh" );
					if ( gotoUrl ) {
						var url = mcw.urlParts( gotoUrl ),
							currentUrl = mcw.urlParts( location.href );

						if ( url.path != currentUrl.path ) {
							if ( $( ".workspace" ).length ) {

								// FIXED ME
								// 一开始replace默认就是取的true，所以需要清查所有地方的remote link
								// 给需要加data-stack-replace的加上，然后这里改成直接判断is data-stack-replace就好了
								var isReplace = true;

								if (link.is('[data-stack-replace]') && link.data('stack-replace') == false) {
									isReplace = false;
								}

								mcw.stack({
									url: gotoUrl,
									replace: isReplace,
									root: link.is('[data-goto-root]'),
									restorePosition: true,
									bare: link.is('[data-goto-bare]')
								});
							} else {
								location.href = gotoUrl;
							}
						}
		            }

					if ( isRefresh ) {
						mcw.stack({
							url: location.href,
							nocache: true
						});
					}
				}
			})
			.on( "ajax:error", $.rails.linkClickSelector, function( e, xhr, status, code ) {
				if (status === 'abort') return;

				var result = $.parseJSON( xhr.responseText ),
					errorMsg = "";

				if ( result.errors ) {
					$.each( result.errors, function( i, err ) {
						errorMsg += err.msg + "<br/>";
					});
				} else {
					errorMsg += result.msg;
				}

				mcw.message({
					msg: errorMsg
				});

			})
			.on( "ajax:beforeSend", $.rails.linkClickSelector, function( e, xhr, settings ) {
				var link = $( this );
				if ( link.is( "[data-global-loading]" )) {
					mcw.globalLoading( link.data( "global-loading" ));
				} else if ( link.is( "[data-loading]" )) {
					mcw.tinyLoading( link );
				}
			});
	}
});
