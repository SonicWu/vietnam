$ ->
  $('.nav-guides a').on('click', (e) ->
    e.preventDefault()

    link = $(e.currentTarget).attr 'href'
    hash = link.substr(1)
    target = $("##{ hash }")

    Sonic.scrollTo(target)
  )
