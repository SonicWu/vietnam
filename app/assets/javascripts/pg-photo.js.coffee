THUMBS_COUNT_PER_PAGE = 15

$ ->
  preview = $('.preview')
  previewImg = preview.find('img.preview-img')
  thumbs = $('.album-preview li.thumb')
  socialBtns = preview.find('.social-share-button')

  $('div.thumbs').on('click', '.thumb', (e) ->
    thumb = $(e.currentTarget)

    if thumb.hasClass('current')
      return

    img = $('img', thumb)
    url = img.data('url')
    src = img.data('origin-src')
    share = img.data('origin-share')
    idx = thumbs.index(thumb)

    unless thumb.is(':visible')
      targetPage = Math.floor(idx / THUMBS_COUNT_PER_PAGE) + 1
      $('#photos-pager').find('a:contains(' + targetPage + ')').click()

    thumb.siblings('.current').removeClass('current')
      .end().addClass('current')

    $('.current-order').text(idx+1)
    
    preview.addClass('loading')
    Sonic.loadImage(src, (img) ->
      preview.removeClass('loading')
      if (img)
        previewImg.attr('src', img.src)
        socialBtns.data('img', share)
          .attr('data-img', share)
      else
        alert('加载失败，请检查您的网络是否有问题')
    )

    if History.enabled
      History.pushState({url: url, idx: idx}, document.title, url)
      History.Adapter.bind(window, 'statechange', (e, f) ->
        state = History.getState()
        if state and state.data and state.data.idx
          thumbs.eq(state.data.idx).click()
      )
  )

  $('.preview-wrap').on('click', '.prev, .next', (e) ->
    target = $(e.target)
    curThumb = $('.thumb.current')

    if target.is('.prev')
      targetThumb = curThumb.prev()

      unless targetThumb.length
        targetThumb = $('.thumb:last')
    else if target.is('.next')
      targetThumb = curThumb.next()

      unless targetThumb.length
        targetThumb = $('.thumb:first')

    if targetThumb
      targetThumb.trigger('click')
  )

  $('#photos-pager').pagination(thumbs.length, {
    items_per_page: THUMBS_COUNT_PER_PAGE
    num_display_entries: 5
    link_to: "javascript:;"
    callback: (page, pagerElem) ->
      thumbs.hide()
        .slice(page*THUMBS_COUNT_PER_PAGE, (page+1)*THUMBS_COUNT_PER_PAGE)
        .show()
  })

  $('.thumb.init-current').click()
