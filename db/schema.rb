# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140513160938) do

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "",               :null => false
    t.string   "encrypted_password",     :default => "",               :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.integer  "role",                   :default => 1,                :null => false
    t.boolean  "forem_admin",            :default => false
    t.string   "forem_state",            :default => "pending_review"
    t.boolean  "forem_auto_subscribe",   :default => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "album_lists", :force => true do |t|
    t.string   "name",                      :null => false
    t.integer  "prior",      :default => 0
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "albums", :force => true do |t|
    t.string   "name"
    t.text     "desc"
    t.integer  "category"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "sight_id"
    t.integer  "album_list_id"
  end

  create_table "areas", :force => true do |t|
    t.string   "name"
    t.integer  "woeid"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "cover",                      :null => false
    t.string   "uuid",                       :null => false
    t.string   "desc",       :default => ""
    t.text     "weather"
  end

  create_table "cities", :force => true do |t|
    t.string   "name"
    t.text     "desc"
    t.integer  "woeid"
    t.integer  "area_id"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.text     "content"
    t.string   "cover",                            :null => false
    t.string   "uuid",                             :null => false
    t.text     "weather"
    t.text     "route_info"
    t.text     "hotel_info"
    t.text     "traffic_info"
    t.string   "map"
    t.integer  "prior",         :default => 0
    t.boolean  "has_experts",   :default => false
    t.boolean  "has_homestays", :default => false
  end

  create_table "ckeditor_assets", :force => true do |t|
    t.string   "data_file_name",                  :null => false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    :limit => 30
    t.string   "type",              :limit => 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], :name => "idx_ckeditor_assetable"
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], :name => "idx_ckeditor_assetable_type"

  create_table "contacts", :force => true do |t|
    t.string   "name",             :null => false
    t.string   "chinese_phone",    :null => false
    t.string   "vietnamese_phone"
    t.string   "passport_number"
    t.string   "qq"
    t.string   "wechat"
    t.string   "weibo"
    t.string   "email"
    t.integer  "order_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "gender"
  end

  create_table "experts", :force => true do |t|
    t.string   "name",                       :null => false
    t.string   "cover",                      :null => false
    t.string   "uuid",                       :null => false
    t.string   "desc",       :default => ""
    t.text     "intro"
    t.integer  "city_id",                    :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.text     "contact"
  end

  create_table "flights", :force => true do |t|
    t.string   "number",     :null => false
    t.string   "airline"
    t.datetime "arrive_at",  :null => false
    t.integer  "order_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "forem_categories", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  add_index "forem_categories", ["slug"], :name => "index_forem_categories_on_slug", :unique => true

  create_table "forem_forums", :force => true do |t|
    t.string  "name"
    t.text    "description"
    t.integer "category_id"
    t.integer "views_count", :default => 0
    t.string  "slug"
  end

  add_index "forem_forums", ["slug"], :name => "index_forem_forums_on_slug", :unique => true

  create_table "forem_groups", :force => true do |t|
    t.string "name"
  end

  add_index "forem_groups", ["name"], :name => "index_forem_groups_on_name"

  create_table "forem_memberships", :force => true do |t|
    t.integer "group_id"
    t.integer "member_id"
  end

  add_index "forem_memberships", ["group_id"], :name => "index_forem_memberships_on_group_id"

  create_table "forem_moderator_groups", :force => true do |t|
    t.integer "forum_id"
    t.integer "group_id"
  end

  add_index "forem_moderator_groups", ["forum_id"], :name => "index_forem_moderator_groups_on_forum_id"

  create_table "forem_posts", :force => true do |t|
    t.integer  "topic_id"
    t.text     "text"
    t.integer  "user_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "reply_to_id"
    t.string   "state",       :default => "pending_review"
    t.boolean  "notified",    :default => false
  end

  add_index "forem_posts", ["reply_to_id"], :name => "index_forem_posts_on_reply_to_id"
  add_index "forem_posts", ["state"], :name => "index_forem_posts_on_state"
  add_index "forem_posts", ["topic_id"], :name => "index_forem_posts_on_topic_id"
  add_index "forem_posts", ["user_id"], :name => "index_forem_posts_on_user_id"

  create_table "forem_subscriptions", :force => true do |t|
    t.integer "subscriber_id"
    t.integer "topic_id"
  end

  create_table "forem_topics", :force => true do |t|
    t.integer  "forum_id"
    t.integer  "user_id"
    t.string   "subject"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.boolean  "locked",       :default => false,            :null => false
    t.boolean  "pinned",       :default => false
    t.boolean  "hidden",       :default => false
    t.datetime "last_post_at"
    t.string   "state",        :default => "pending_review"
    t.integer  "views_count",  :default => 0
    t.string   "slug"
  end

  add_index "forem_topics", ["forum_id"], :name => "index_forem_topics_on_forum_id"
  add_index "forem_topics", ["slug"], :name => "index_forem_topics_on_slug", :unique => true
  add_index "forem_topics", ["state"], :name => "index_forem_topics_on_state"
  add_index "forem_topics", ["user_id"], :name => "index_forem_topics_on_user_id"

  create_table "forem_views", :force => true do |t|
    t.integer  "user_id"
    t.integer  "viewable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "count",             :default => 0
    t.string   "viewable_type"
    t.datetime "current_viewed_at"
    t.datetime "past_viewed_at"
  end

  add_index "forem_views", ["updated_at"], :name => "index_forem_views_on_updated_at"
  add_index "forem_views", ["user_id"], :name => "index_forem_views_on_user_id"
  add_index "forem_views", ["viewable_id"], :name => "index_forem_views_on_topic_id"

  create_table "guides", :force => true do |t|
    t.string   "question"
    t.text     "answer"
    t.integer  "category"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "prior",      :default => 0
    t.boolean  "hot",        :default => false
  end

  create_table "homestays", :force => true do |t|
    t.string   "name",                       :null => false
    t.string   "cover"
    t.string   "uuid",                       :null => false
    t.string   "desc",       :default => ""
    t.text     "intro"
    t.integer  "city_id"
    t.integer  "price"
    t.string   "contact"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "hotels", :force => true do |t|
    t.string   "name",       :null => false
    t.string   "address",    :null => false
    t.string   "phone"
    t.integer  "order_id"
    t.integer  "flight_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "info_items", :force => true do |t|
    t.string   "name",                      :null => false
    t.string   "cover"
    t.string   "link"
    t.integer  "info_id",                   :null => false
    t.string   "uuid",                      :null => false
    t.integer  "prior",      :default => 0
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "infos", :force => true do |t|
    t.string   "name",                      :null => false
    t.string   "cover",                     :null => false
    t.string   "link"
    t.integer  "category"
    t.string   "uuid",                      :null => false
    t.integer  "prior",      :default => 0
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "orders", :force => true do |t|
    t.integer  "scheme_id"
    t.datetime "tour_time"
    t.integer  "tour_members_count",                    :null => false
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
    t.integer  "tour_children_free",     :default => 0
    t.integer  "tour_children_discount", :default => 0
    t.integer  "status",                                :null => false
    t.integer  "category",                              :null => false
    t.string   "cancel_reason"
    t.integer  "tour_adult_count",       :default => 0
  end

  create_table "photos", :force => true do |t|
    t.string   "name"
    t.string   "desc"
    t.string   "uuid",                         :null => false
    t.integer  "album_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "file",       :default => ""
    t.string   "link"
    t.boolean  "horizental", :default => true
  end

  create_table "schemes", :force => true do |t|
    t.string   "name",                                                               :null => false
    t.string   "cover",                                                              :null => false
    t.integer  "price",                                           :default => 0,     :null => false
    t.text     "content"
    t.decimal  "discount",         :precision => 10, :scale => 0
    t.boolean  "hot",                                             :default => false
    t.datetime "created_at",                                                         :null => false
    t.datetime "updated_at",                                                         :null => false
    t.string   "area_id"
    t.string   "desc",                                            :default => ""
    t.string   "uuid",                                                               :null => false
    t.integer  "travel_agency_id"
  end

  create_table "schemesights", :force => true do |t|
    t.integer  "scheme_id"
    t.integer  "sight_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "settles", :force => true do |t|
    t.date     "date"
    t.integer  "scheme_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sights", :force => true do |t|
    t.string   "name"
    t.string   "desc",       :default => ""
    t.text     "content"
    t.integer  "city_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.string   "cover",                      :null => false
    t.string   "uuid",                       :null => false
    t.integer  "category",   :default => 0
  end

  create_table "sites", :force => true do |t|
    t.text "about_us"
    t.text "disclaimer"
    t.text "contact_us"
    t.text "experts_intro"
    t.text "homestays_intro"
  end

  create_table "travel_agencies", :force => true do |t|
    t.string   "name",          :null => false
    t.string   "contact_name"
    t.string   "contact_phone"
    t.string   "contact_qq"
    t.string   "contact_email"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

end
