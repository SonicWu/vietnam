class AddCategoryToSights < ActiveRecord::Migration
  def change
    add_column :sights, :category, :integer, :default => 0
  end
end
