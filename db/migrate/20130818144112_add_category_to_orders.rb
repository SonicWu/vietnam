class AddCategoryToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :category, :integer, :null => false
  end
end
