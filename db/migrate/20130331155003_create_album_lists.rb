class CreateAlbumLists < ActiveRecord::Migration
  def change
    create_table :album_lists do |t|
      t.string :name, :null => false
      t.integer :prior, :default => 0

      t.timestamps
    end
  end
end
