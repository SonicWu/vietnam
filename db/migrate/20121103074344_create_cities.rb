# -*- encoding : utf-8 -*-
class CreateCities < ActiveRecord::Migration
  def change
    create_table :cities do |t|
      t.string :name
      t.text :desc
      t.integer :woeid
      t.integer :area_id

      t.timestamps
    end
  end
end
