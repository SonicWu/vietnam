class CreateInfoItems < ActiveRecord::Migration
  def change
    create_table :info_items do |t|
      t.string :name, :null => false
      t.string :cover
      t.string :link
      t.integer :info_id, :null => false
      t.string :uuid, :null => false
      t.integer :prior, :default => 0

      t.timestamps
    end
  end

end
