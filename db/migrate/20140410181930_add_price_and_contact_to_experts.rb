class AddPriceAndContactToExperts < ActiveRecord::Migration
  def change
    add_column :experts, :price, :integer
    add_column :experts, :contact, :string
  end
end
