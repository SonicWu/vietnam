class AddHasHomestaysToCities < ActiveRecord::Migration
  def change
    add_column :cities, :has_homestays, :boolean, :default => false
  end
end
