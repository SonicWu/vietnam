class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :scheme_id, :null => false
      t.datetime :tour_time, :null => false
      t.integer :tour_members_count, :null => false
      t.string :tour_children_info
      t.string :tour_oldman_info
      t.timestamps
    end

    create_table :contacts do |t|
      t.string :name, :null => false
      t.string :chinese_phone, :null => false
      t.string :vietnamese_phone
      t.string :passport_number
      t.string :qq
      t.string :wechat
      t.string :weibo
      t.string :email
      t.belongs_to :order
      t.timestamps
    end

    create_table :hotels do |t|
      t.string :name, :null => false
      t.string :address, :null => false
      t.string :phone, :null => false
      t.belongs_to :order
      t.belongs_to :flight
      t.timestamps
    end

    create_table :flights do |t|
      t.string :number, :null => false
      t.string :airline, :null => false
      t.datetime :arrive_at, :null => false
      t.belongs_to :order
      t.timestamps
    end
  end
end
