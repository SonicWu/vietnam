class AddDescToSchemes < ActiveRecord::Migration
  def change
    add_column :schemes, :desc, :string, :default => ''
  end
end
