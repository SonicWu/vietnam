class AddPriorToGuides < ActiveRecord::Migration
  def change
    add_column :guides, :prior, :integer, :default => 0
  end
end
