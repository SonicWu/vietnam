class ChangeTourOldmanInfoToTourAdultCount < ActiveRecord::Migration
  def change
    remove_column :orders, :tour_oldman_info
    add_column :orders, :tour_adult_count, :integer, :default => 0
  end
end
