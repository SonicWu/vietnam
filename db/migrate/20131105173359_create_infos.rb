class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.string :name, :null => false
      t.string :cover, :null => false
      t.string :link
      t.integer :category
      t.string :uuid, :null => false
      t.integer :prior, :default => 0

      t.timestamps
    end
  end
end
