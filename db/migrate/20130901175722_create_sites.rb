class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.text :about_us
      t.text :disclaimer
    end
  end
end
