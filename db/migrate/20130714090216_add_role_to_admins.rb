class AddRoleToAdmins < ActiveRecord::Migration
  def change
    add_column :admin_users, :role, :integer, :null => false, :default => 1
  end
end
