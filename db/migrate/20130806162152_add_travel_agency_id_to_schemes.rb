class AddTravelAgencyIdToSchemes < ActiveRecord::Migration
  def change
    add_column :schemes, :travel_agency_id, :integer
  end
end
