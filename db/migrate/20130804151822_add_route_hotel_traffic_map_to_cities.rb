class AddRouteHotelTrafficMapToCities < ActiveRecord::Migration
  def change
    add_column :cities, :route_info, :text
    add_column :cities, :hotel_info, :text
    add_column :cities, :traffic_info, :text
    add_column :cities, :map, :string
  end
end
