class AddDescToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :desc, :string, :default => ''
  end
end
