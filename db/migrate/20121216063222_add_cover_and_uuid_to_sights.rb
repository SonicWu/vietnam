class AddCoverAndUuidToSights < ActiveRecord::Migration
  def change
    add_column :sights, :cover, :string, :null => false
    add_column :sights, :uuid, :string, :null => false
  end
end
