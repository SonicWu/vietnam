# -*- encoding : utf-8 -*-
class AddFileToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :file, :string, :default => ''
  end
end
