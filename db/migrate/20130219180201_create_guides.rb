# -*- encoding : utf-8 -*-
class CreateGuides < ActiveRecord::Migration
  def change
    create_table :guides do |t|
      t.string  :question
      t.text    :answer
      t.integer :category

      t.timestamps
    end
  end
end
