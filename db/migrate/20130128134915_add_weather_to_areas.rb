class AddWeatherToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :weather, :text, :default => ''
  end
end
