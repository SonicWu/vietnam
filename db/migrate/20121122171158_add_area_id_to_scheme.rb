# -*- encoding : utf-8 -*-
class AddAreaIdToScheme < ActiveRecord::Migration
  def change
    add_column :schemes, :area_id, :string
  end
end
