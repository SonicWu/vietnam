class CreateTravelAgencies < ActiveRecord::Migration
  def change
    create_table :travel_agencies do |t|
      t.string :name, :null => false
      t.string :contact_name
      t.string :contact_phone
      t.string :contact_qq
      t.string :contact_email

      t.timestamps
    end
  end
end
