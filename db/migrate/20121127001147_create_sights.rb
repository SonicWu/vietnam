# -*- encoding : utf-8 -*-
class CreateSights < ActiveRecord::Migration
  def change
    create_table :sights do |t|
      t.string :name, :Null => false
      t.string :desc, :default => ''
      t.text :content, :default => ''
      t.integer :woeid
      t.integer :city_id

      t.timestamps
    end
  end
end
