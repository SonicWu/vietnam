class ChangePriceContactToExperts < ActiveRecord::Migration
  def change
    remove_column :experts, :price
    change_column :experts, :contact, :text
  end
end
