class CreateExperts < ActiveRecord::Migration
  def change
    create_table :experts do |t|
      t.string  :name,     :null => false
      t.string  :cover,    :null => false
      t.string  :uuid,     :null => false
      t.string  :desc,     :default => ''
      t.text    :intro,    :defualt => ''
      t.integer :city_id,  :null => false

      t.timestamps
    end
  end
end
