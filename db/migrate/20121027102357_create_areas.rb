# -*- encoding : utf-8 -*-
class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.string :name
      t.integer :woeid

      t.timestamps
    end
  end
end
