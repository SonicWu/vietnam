# -*- encoding : utf-8 -*-
class CreateSchemes < ActiveRecord::Migration
  def change
    create_table :schemes do |t|
      t.string :name, :null => false
      t.string :cover, :null => false
      t.integer :price, :null => false, :default => 0
      t.text :content
      t.decimal :discount
      t.boolean :hot, :default => false

      t.timestamps
    end
  end
end
