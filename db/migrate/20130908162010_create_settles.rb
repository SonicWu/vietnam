class CreateSettles < ActiveRecord::Migration
  def change
    create_table :settles do |t|
      t.date :date
      t.integer :scheme_id

      t.timestamps
    end
  end
end
