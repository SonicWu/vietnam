class AddExpertsIntroToSite < ActiveRecord::Migration
  def change
    add_column :sites, :experts_intro, :text
  end
end
