class ChangeFlightAirlineAllowNull < ActiveRecord::Migration
  def change
    change_column :flights, :airline, :string, :null => true
  end
end
