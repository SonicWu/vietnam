class AddCoverAndUuidToAreas < ActiveRecord::Migration
  def change
    add_column :areas, :cover, :string, :null => false
    add_column :areas, :uuid, :string, :null => false
  end
end
