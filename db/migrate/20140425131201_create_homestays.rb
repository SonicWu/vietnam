class CreateHomestays < ActiveRecord::Migration
  def change
    create_table :homestays do |t|
      t.string  :name,     :null => false
      t.string  :cover
      t.string  :uuid,     :null => false
      t.string  :desc,     :default => ''
      t.text    :intro,    :defualt => ''
      t.integer :city_id
      t.integer :price
      t.string  :contact

      t.timestamps
    end
  end
end
