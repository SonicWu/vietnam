class AddUuidToSchemes < ActiveRecord::Migration
  def change
    add_column :schemes, :uuid, :string, :null => false
  end
end
