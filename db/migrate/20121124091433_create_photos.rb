# -*- encoding : utf-8 -*-
class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :name, :null => false
      t.string :desc
      t.string :uuid, :null => false
      t.integer :album_id

      t.timestamps
    end
  end
end
