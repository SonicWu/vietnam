class AddContactUsToSites < ActiveRecord::Migration
  def change
    add_column :sites, :contact_us, :text
  end
end
