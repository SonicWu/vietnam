class SchemeSight < ActiveRecord::Migration
  def change
    create_table :schemesights do |t|
      t.integer :scheme_id
      t.integer :sight_id

      t.timestamps
    end
  end
end
