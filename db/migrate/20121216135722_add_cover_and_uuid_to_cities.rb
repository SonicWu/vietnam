class AddCoverAndUuidToCities < ActiveRecord::Migration
  def change
    add_column :cities, :cover, :string, :null => false
    add_column :cities, :uuid, :string, :null => false
  end
end
