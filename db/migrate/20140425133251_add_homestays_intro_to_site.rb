class AddHomestaysIntroToSite < ActiveRecord::Migration
  def change
    add_column :sites, :homestays_intro, :text
  end
end
