class AddPriorToCities < ActiveRecord::Migration
  def change
    add_column :cities, :prior, :integer, :default => 0
  end
end
