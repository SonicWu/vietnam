class ChangeHotelsPhoneAllowNull < ActiveRecord::Migration
  def change
    change_column :hotels, :phone, :string, :null => true
  end
end
