class ModifyChildrenInfoOfOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :tour_children_info
    add_column :orders, :tour_children_free, :integer, :default => 0
    add_column :orders, :tour_children_discount, :integer, :default => 0
  end
end
