# -*- encoding : utf-8 -*-
class AddContentToCities < ActiveRecord::Migration
  def change
    add_column :cities, :content, :text, :default => ''
  end
end
