class AddHasExpertsToCities < ActiveRecord::Migration
  def change
    add_column :cities, :has_experts, :boolean, :default => false
  end
end
