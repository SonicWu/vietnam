class AddHotToGuides < ActiveRecord::Migration
  def change
    add_column :guides, :hot, :boolean, :default => false
  end
end
