class AddAlbumListIdToAlbum < ActiveRecord::Migration
  def change
    add_column :albums, :album_list_id, :integer
  end
end
