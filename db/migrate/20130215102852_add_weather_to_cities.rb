class AddWeatherToCities < ActiveRecord::Migration
  def change
    add_column :cities, :weather, :text, :default => ''
  end
end
