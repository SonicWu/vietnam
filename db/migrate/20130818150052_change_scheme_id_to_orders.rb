class ChangeSchemeIdToOrders < ActiveRecord::Migration
  def change
    change_column :orders, :scheme_id, :integer, :null => true
    change_column :orders, :tour_time, :datetime, :null => true
  end
end
