# -*- encoding : utf-8 -*-
Vietnam::Application.routes.draw do
  root :to => 'home#index'
  get "guides/index"
  get "about_us"      => 'home#about_us', as: :about_us
  get "contact_us"      => 'home#contact_us', as: :contact_us

  mount Ckeditor::Engine => '/ckeditor'
  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config

  resources :areas, :cities, :albums, :schemes, :sights, :photos, :only => [:show]
  resources :experts, :homestays, only: [:index, :show]
  # resources :orders
  get '/infos', :to => 'infos#index'
  get '/infos/:id', :to => 'infos#show', as: :info
  get '/guides', :to => 'guides#index'
  # get '/girls', :to => 'home#girls', as: :girls
  # get '/girls/:id', :to => 'home#girl', as: :girl


  get 'orders/settle/'                => 'orders#settle', as: :settle_order
  post 'orders/settle/'              => 'orders#set_settle', as: :set_settle
  get 'orders/'                    =>  'orders#index'
  get 'orders/new/'                =>  'orders#new', as: :new_order
  get 'orders/new/pickup'          =>  'orders#new_pickup'
  get 'orders/new/tour'            =>  'orders#new_tour'
  post 'orders'                     =>  'orders#create'
  get 'orders/:id/'                 => 'orders#show',            as: :order
  get 'orders/:id/edit'             =>  'orders#edit',           as: :edit_order
  post 'orders/:id/'                => 'orders#update',          as: :update_order
  delete 'orders/:id/'              => 'orders#destroy',         as: :destroy_order

  get 'wedding'            => 'home#wedding', as: :wedding
  get 'tailor'             => 'home#tailor',  as: :tailor
  get 'charter'            => 'home#charter', as: :charter
end
