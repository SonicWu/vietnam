Photo.all.each do |item|
  file = item.file.file
  path = file.path
  dir = File.dirname(file.path)
  name = "#{ item.uuid }.#{ file.extension }"
  new_path = "#{ dir }/#{ name }"
  begin
    # File.try(:rename, path, new_path)
    item.file = File.open(new_path)
    item.save!
  rescue
    print "FAIL: #{ item.name }"
  end
end
