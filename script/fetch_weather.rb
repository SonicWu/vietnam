(Area.all + City.all).each do |area|
  woeid = area.woeid
  next if woeid.nil? or woeid.blank?

  client = Weatherman::Client.new :lang => 'zh-cn'
  res = client.lookup_by_woeid woeid

  img_src = res.description_image['src']

  area.weather = %{
    <div class=\"weather\">
      <img src=\"#{ img_src }\" />
      <p>#{ res.condition['text'] }</p>
      <p>温度: #{ res.condition['temp'] }℃</p>
      <p>明天: #{ res.forecasts.first['low'] }℃ - #{ res.forecasts.first['high'] }℃</p>
      <p>后天: #{ res.forecasts.second['low'] }℃ - #{ res.forecasts.second['high'] }℃</p>
    </div>
  }

  area.save!
end
